package bicca.lucas.woopsicredi

import bicca.lucas.woopsicredi.service.converter.EventConverter
import bicca.lucas.woopsicredi.service.response.EventResponse
import org.junit.Test

class EventConverterTest {

    @Test
    fun mainInfosNotEmpty() {
        val eventResponse = EventResponse("2", "Mobile Summit", "-12.3456", "-12.3456", "image", "Mobile Summit", 11, emptyList())
        val event = EventConverter.converter(eventResponse)
        assert(!event.eventId.isEmpty())
        assert(!event.title.isEmpty())
        assert(!event.latitude.isEmpty())
        assert(!event.longitude.isEmpty())
        assert(!event.image.isEmpty())
        assert(!event.description.isEmpty())
        assert(!event.date.isEmpty())
    }
}