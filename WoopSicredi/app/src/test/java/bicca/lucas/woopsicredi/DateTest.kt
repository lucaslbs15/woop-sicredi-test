package bicca.lucas.woopsicredi

import bicca.lucas.woopsicredi.util.DateFormatUtil
import org.junit.Assert
import org.junit.Test
import java.util.*

class DateTest {

    @Test
    fun dateValid() {
        val date = Date()
        Assert.assertEquals("31/08/2018", DateFormatUtil.fromLong(date.time, "dd/MM/yyyy"))
    }
}