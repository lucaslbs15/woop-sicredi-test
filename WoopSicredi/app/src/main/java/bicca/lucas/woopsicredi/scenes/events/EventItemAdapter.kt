package bicca.lucas.woopsicredi.scenes.events

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import bicca.lucas.woopsicredi.BR
import bicca.lucas.woopsicredi.R
import bicca.lucas.woopsicredi.databinding.ItemEventBinding
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.scenes.RecyclerViewHolder
import bicca.lucas.woopsicredi.scenes.RecyclerViewItemClickListener
import com.bumptech.glide.Glide

class EventItemAdapter(private val context: Context,
                       private val itemClickListener: RecyclerViewItemClickListener,
                       private val items: List<Event>) : RecyclerView.Adapter<RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemEventBinding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return RecyclerViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = items.get(position)
        holder.bind(item, BR.itemEventModel)
        Glide.with(context).load(item.image).into((holder.binding as ItemEventBinding).imageView)
        (holder.binding as ItemEventBinding).itemEventRoot.setOnClickListener {
            itemClickListener.onRecyclerViewItemClick(it, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_event
    }


}