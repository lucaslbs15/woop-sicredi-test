package bicca.lucas.woopsicredi.scenes

import android.view.View

interface RecyclerViewItemClickListener {
    fun onRecyclerViewItemClick(view: View?, position: Int)
}