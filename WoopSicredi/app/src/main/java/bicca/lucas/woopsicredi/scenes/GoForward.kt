package bicca.lucas.woopsicredi.scenes

import android.content.Intent

interface GoForward {
    fun nextActivity(intent: Intent)
}