package bicca.lucas.woopsicredi.model

class People(val id: String,
             val eventId: String,
             val name: String,
             val picture: String)