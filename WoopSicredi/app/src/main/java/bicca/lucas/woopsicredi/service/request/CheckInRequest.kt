package bicca.lucas.woopsicredi.service.request

class CheckInRequest(val eventId: String,
                     val name: String,
                     val email: String)