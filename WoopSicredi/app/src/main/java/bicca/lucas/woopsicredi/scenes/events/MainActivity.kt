package bicca.lucas.woopsicredi.scenes.events

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import bicca.lucas.woopsicredi.R
import bicca.lucas.woopsicredi.databinding.ActivityMainBinding
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.scenes.GoForward
import bicca.lucas.woopsicredi.scenes.RecyclerViewItemClickListener
import bicca.lucas.woopsicredi.scenes.details.DetailsActivity

class MainActivity : AppCompatActivity(), GoForward, RecyclerViewItemClickListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var dataBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        dataBinding.viewModel = viewModel
        initLiveData()
    }

    override fun onResume() {
        super.onResume()
        viewModel.callGetEvents()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposable?.dispose()
    }

    private fun showList(response: List<Event>) {
        dataBinding.activityMainAnimation.visibility = View.GONE
        dataBinding.activityMainRecyclerview.visibility = View.VISIBLE
        dataBinding.activityMainRecyclerview.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 2)
        dataBinding.activityMainRecyclerview.layoutManager = layoutManager
        val adapter = EventItemAdapter(this, this, response)
        dataBinding.activityMainRecyclerview.adapter = adapter
    }

    private fun initLiveData() {
        initLiveDataSuccess()
        initLiveDataFailure()
        initLiveDataRefresh()
    }

    private fun initLiveDataSuccess() {
        val eventsObserver = Observer<List<Event>> { result ->
            if (result != null) {
                showList(result)
            }
        }
        viewModel.successLiveData.observe(this, eventsObserver)
    }

    private fun initLiveDataFailure() {
        val failureObserver = Observer<String> { result ->
            if (result != null) {
                dataBinding.activityMainAnimation.visibility = View.GONE
                Toast.makeText(this,
                        resources.getString(R.string.events_failure_request),
                        Toast.LENGTH_LONG).show()
            }
        }
        viewModel.errorLiveData.observe(this, failureObserver)
    }

    private fun initLiveDataRefresh() {
        val refreshObserver = Observer<Boolean> { result ->
            if (result != null && result) {
                dataBinding.activityMainAnimation.visibility = View.VISIBLE
                dataBinding.activityMainRecyclerview.visibility = View.GONE
                viewModel.callGetEvents()
            }
        }
        viewModel.refreshLiveData.observe(this, refreshObserver)
    }

    override fun nextActivity(intent: Intent) {
        startActivity(intent)
    }

    override fun onRecyclerViewItemClick(view: View?, position: Int) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(resources.getString(R.string.intent_event_id), viewModel.getEvent(position)?.eventId)
        nextActivity(intent)
    }
}
