package bicca.lucas.woopsicredi.service

import bicca.lucas.woopsicredi.service.request.CheckInRequest
import bicca.lucas.woopsicredi.service.response.EventResponse
import bicca.lucas.woopsicredi.service.response.Response
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EventApiService {

    @GET("/api/events")
    fun getEvents() : Observable<List<EventResponse>>

    @GET("/api/events/{id}")
    fun getEvent(@Path("id") id: String) : Observable<EventResponse>

    @POST("api/checkin")
    fun doCheckIn(@Body checkInRequest: CheckInRequest) : Observable<Response>

    companion object {
        fun create() : EventApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("http://5b840ba5db24a100142dcd8c.mockapi.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(EventApiService::class.java)
        }
    }
}