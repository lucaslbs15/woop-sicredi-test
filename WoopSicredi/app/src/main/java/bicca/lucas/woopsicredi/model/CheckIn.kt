package bicca.lucas.woopsicredi.model

import android.databinding.Bindable
import android.databinding.Observable
import android.databinding.PropertyChangeRegistry
import bicca.lucas.woopsicredi.BR

class CheckIn : Observable {

    private val propertyChangeRegistry = PropertyChangeRegistry()

    var eventId: String = ""
    @Bindable get
    set(value) {
        field = value
        propertyChangeRegistry.notifyChange(this, BR.eventId)
    }

    var name: String = ""
    @Bindable get
    set(value) {
        field = value
        propertyChangeRegistry.notifyChange(this, BR.name)
    }

    var email: String = ""
    @Bindable get
    set(value) {
        field = value
        propertyChangeRegistry.notifyChange(this, BR.email)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        propertyChangeRegistry.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        propertyChangeRegistry.add(callback)
    }

}