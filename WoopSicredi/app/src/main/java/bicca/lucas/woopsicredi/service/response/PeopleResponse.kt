package bicca.lucas.woopsicredi.service.response

class PeopleResponse(val id: String,
                     val eventId: String,
                     val name: String,
                     val picture: String)