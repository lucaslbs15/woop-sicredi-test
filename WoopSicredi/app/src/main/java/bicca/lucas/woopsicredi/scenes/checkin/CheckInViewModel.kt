package bicca.lucas.woopsicredi.scenes.checkin

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import bicca.lucas.woopsicredi.model.CheckIn
import bicca.lucas.woopsicredi.scenes.ViewModelBase
import bicca.lucas.woopsicredi.service.converter.CheckInConverter
import bicca.lucas.woopsicredi.service.request.CheckInRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckInViewModel : ViewModelBase<Boolean>() {

    val observableField = ObservableField<CheckIn>()
    lateinit var eventId: String
    val checkInLiveDate = MutableLiveData<Boolean>()

    init {
        observableField.set(CheckIn())
    }

    fun callCheckIn(checkInRequest: CheckInRequest) {
        disposable = eventApiService.doCheckIn(checkInRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    successLiveData.value = result.code.equals("200")
                }, { error ->
                    errorLiveData.value = error.localizedMessage
                })
    }

    fun doCheckInClick() {
        checkInLiveDate.value = true
        observableField.get()?.eventId = eventId
        val checkInRequest = CheckInConverter.converter(observableField.get()!!)
        callCheckIn(checkInRequest)
    }

}