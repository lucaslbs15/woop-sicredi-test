package bicca.lucas.woopsicredi.scenes

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BaseObservable
import bicca.lucas.woopsicredi.service.EventApiService
import io.reactivex.disposables.Disposable

open class ViewModelBase<T> : ViewModel() {

    val eventApiService by lazy {
        EventApiService.create()
    }

    var disposable: Disposable? = null

    val successLiveData = MutableLiveData<T>()

    val errorLiveData = MutableLiveData<String>()

}