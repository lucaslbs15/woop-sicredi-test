package bicca.lucas.woopsicredi.service.converter

import bicca.lucas.woopsicredi.util.DateFormatUtil
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.model.People
import bicca.lucas.woopsicredi.service.response.EventResponse

object EventConverter {

    fun converter(response: List<EventResponse>) : List<Event> {
        val events: ArrayList<Event> = ArrayList()
        for (item in response) {
            events.add(converter(item))
        }
        return events
    }

    fun converter(response: EventResponse) : Event {
        val peoples: ArrayList<People> = ArrayList()
        for (item in response.people) {
            val people = People(item.id, item.eventId, item.name, item.picture)
            peoples.add(people)
        }
        val event = Event()
        event.eventId = response.id
        event.title = response.title
        event.latitude = response.latitude
        event.longitude = response.longitude
        event.image = response.image
        event.description = response.description
        event.date = DateFormatUtil.fromLong(response.date, "MM/dd/yyyy HH:mm")
        event.peoples = peoples

        return event
    }
}