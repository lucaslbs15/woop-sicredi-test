package bicca.lucas.woopsicredi.scenes.events

import android.arch.lifecycle.MutableLiveData
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.scenes.ViewModelBase
import bicca.lucas.woopsicredi.service.converter.EventConverter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel: ViewModelBase<List<Event>>() {

    lateinit var events: List<Event>
    val refreshLiveData = MutableLiveData<Boolean>()

    fun callGetEvents() {
        disposable = eventApiService.getEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    events = EventConverter.converter(result)
                    successLiveData.value = events
                }, { error ->
                    errorLiveData.value = error.localizedMessage
                })
    }

    fun getEvent(position: Int) : Event? = events.get(position)

    fun onRefreshClick() {
        refreshLiveData.value = true
    }
}