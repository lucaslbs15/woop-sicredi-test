package bicca.lucas.woopsicredi.scenes.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import bicca.lucas.woopsicredi.R
import bicca.lucas.woopsicredi.databinding.ActivityDetailsBinding
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.scenes.GoForward
import bicca.lucas.woopsicredi.scenes.checkin.CheckInActivity
import com.bumptech.glide.Glide

class DetailsActivity : AppCompatActivity(), GoForward {

    private lateinit var viewModel: DetailsViewModel
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        viewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        binding.detailsViewModel = viewModel
        viewModel.callGetEvent(getExtraString())
        initLiveData()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposable?.dispose()
    }

    private fun getExtraString(): String {
        if (intent != null
                && intent.getStringExtra(resources.getString(R.string.intent_event_id)) != null) {
            return intent.getStringExtra(resources.getString(R.string.intent_event_id))
        }
        return ""
    }

    private fun showImage(event: Event) {
        binding.activityDetailsImage.visibility = View.VISIBLE
        Glide.with(this).load(event.image).into(binding.activityDetailsImage)
    }

    private fun initLiveData() {
        initLiveDataSucess()
        initLiveDataFailure()
        initLiveDataCheckIn()
        initLiveDataOpenMaps()
    }

    private fun initLiveDataSucess() {
        val eventObserver = Observer<Event> { result ->
            if (result != null) {
                binding.activityDetailsAnimation.visibility = View.GONE
                binding.scrollView3.visibility = View.VISIBLE
                showImage(result)
            }
        }
        viewModel.successLiveData.observe(this, eventObserver)
    }

    private fun initLiveDataFailure() {
        val failureObserver = Observer<String> { result ->
            if (result != null) {
                Toast.makeText(this,
                        resources.getString(R.string.details_failure_request),
                        Toast.LENGTH_LONG).show()
            }
        }
        viewModel.errorLiveData.observe(this, failureObserver)
    }

    private fun initLiveDataCheckIn() {
        val checkInObserver = Observer<String> {  result ->
            if (result != null) {
                val intent = Intent(this, CheckInActivity::class.java)
                intent.putExtra(resources.getString(R.string.intent_event_id), result)
                nextActivity(intent)
            }
        }
        viewModel.checkInLiveData.observe(this, checkInObserver)
    }

    private fun initLiveDataOpenMaps() {
        val openMapsObserver = Observer<Boolean> { result ->
            if (result != null && result) {
                openMapsIntent(viewModel.event.get()?.latitude!!, viewModel.event.get()?.longitude!!)
            }
        }
        viewModel.openMapsLiveData.observe(this, openMapsObserver)
    }

    private fun openMapsIntent(lat: String, long: String) {
        val uri = Uri.parse("geo:${lat},${long}?q=${lat},${long}(${viewModel.event.get()?.title})")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.setPackage("com.google.android.apps.maps")
        nextActivity(intent)
    }

    override fun nextActivity(intent: Intent) {
        startActivity(intent)
    }
}
