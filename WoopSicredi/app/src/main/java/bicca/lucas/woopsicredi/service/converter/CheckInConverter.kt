package bicca.lucas.woopsicredi.service.converter

import bicca.lucas.woopsicredi.model.CheckIn
import bicca.lucas.woopsicredi.service.request.CheckInRequest

object CheckInConverter {

    fun converter(checkIn: CheckIn) : CheckInRequest {
        val checkInRequest = CheckInRequest(checkIn.eventId, checkIn.name, checkIn.email)
        return checkInRequest
    }
}