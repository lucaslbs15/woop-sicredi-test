package bicca.lucas.woopsicredi.scenes.checkin

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import bicca.lucas.woopsicredi.R
import bicca.lucas.woopsicredi.databinding.ActivityCheckInBinding
import bicca.lucas.woopsicredi.scenes.GoForward
import bicca.lucas.woopsicredi.service.request.CheckInRequest

class CheckInActivity : AppCompatActivity(), GoForward {

    private lateinit var viewModel: CheckInViewModel
    private lateinit var binding: ActivityCheckInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_in)
        viewModel = ViewModelProviders.of(this).get(CheckInViewModel::class.java)
        viewModel.eventId = getExtraString()
        binding.checkInViewModel = viewModel
        initLiveData()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disposable?.dispose()
    }

    private fun getExtraString(): String {
        if (intent != null
                && intent.getStringExtra(resources.getString(R.string.intent_event_id)) != null) {
            return intent.getStringExtra(resources.getString(R.string.intent_event_id))
        }
        return ""
    }

    private fun initLiveData() {
        initLiveDataSuccess()
        initLiveDataFailure()
        initLiveDataCheckIn()
    }

    private fun initLiveDataSuccess() {
        val checkInObserver = Observer<Boolean> { result ->
            if (result != null) {
                Toast.makeText(this,
                        resources.getString(R.string.checkin_success_request),
                        Toast.LENGTH_LONG).show()
                finish()
            }
        }
        viewModel.successLiveData.observe(this, checkInObserver)
    }

    private fun initLiveDataFailure() {
        val failureObserver = Observer<String> { result ->
            if (result != null) {
                Toast.makeText(this,
                        resources.getString(R.string.checkin_failure_request),
                        Toast.LENGTH_LONG).show()
            }
        }
        viewModel.errorLiveData.observe(this, failureObserver)
    }

    private fun initLiveDataCheckIn() {
        val checkInObserver = Observer<Boolean> { result ->
            if (result != null && result) {
                binding.activityCheckinAnimation.visibility = View.VISIBLE
            }
        }
        viewModel.checkInLiveDate.observe(this, checkInObserver)
    }

    override fun nextActivity(intent: Intent) {
        //TODO implementar a animação de check in finalizado.
    }
}
