package bicca.lucas.woopsicredi.scenes

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

class RecyclerViewHolder(val binding: ViewDataBinding)
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(obj: Any, variableId: Int) {
        binding.setVariable(variableId, obj)
        binding.executePendingBindings()
    }
}