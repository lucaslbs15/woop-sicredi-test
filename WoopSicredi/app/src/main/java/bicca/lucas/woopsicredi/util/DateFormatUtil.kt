package bicca.lucas.woopsicredi.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object DateFormatUtil {

    fun fromLong(date: Long, format: String) : String {
        val dateObject = Date(date)
        val format: DateFormat = SimpleDateFormat(format)
        return format.format(dateObject)
    }
}