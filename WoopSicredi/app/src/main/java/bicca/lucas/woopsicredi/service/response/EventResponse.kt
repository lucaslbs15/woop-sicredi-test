package bicca.lucas.woopsicredi.service.response

class EventResponse(val id: String,
                    val title: String,
                    val latitude: String,
                    val longitude: String,
                    val image: String,
                    val description: String,
                    val date: Long,
                    val people: List<PeopleResponse>)