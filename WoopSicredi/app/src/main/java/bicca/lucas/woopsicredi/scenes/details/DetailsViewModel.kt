package bicca.lucas.woopsicredi.scenes.details

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import bicca.lucas.woopsicredi.model.Event
import bicca.lucas.woopsicredi.scenes.ViewModelBase
import bicca.lucas.woopsicredi.service.converter.EventConverter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailsViewModel : ViewModelBase<Event>() {

    val checkInLiveData = MutableLiveData<String>()
    val openMapsLiveData = MutableLiveData<Boolean>()
    lateinit var eventId: String
    var event = ObservableField<Event>()

    fun callGetEvent(eventId: String) {
        this.eventId = eventId
        disposable = eventApiService.getEvent(eventId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    event.set(EventConverter.converter(result))
                    successLiveData.value = event.get()
                }, { error ->
                    errorLiveData.value = error.localizedMessage
                })
    }

    fun onCheckInClick() {
        checkInLiveData.value = eventId
    }

    fun onOpenMaps() {
        openMapsLiveData.value = true
    }
}