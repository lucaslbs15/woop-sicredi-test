- Foi utilizado a biblioteca Retrofit para fazer requests em API REST. 
A escolha foi pelo fato de que esta possui uma fácil implementação e uma abstração reduzindo código.
Outro motivo é que a lib permite trabalhar com o RxJava facilmente. Foi utilizado Rx com o Retrofit,
tendo como principal vantagem a ausência de callbacks, evitando problemas de crashes.

- Para exibição de imagens, foi utilizado a biblioteca Glide. Foi utilizada para exibir imagens a partir de URLs.

- Para evitar o boilerplate na Activity com os "findViewById", foi utilizado DataBinding. 
Além de diminuir o boilerplate das referências de ids, é possível adicionar models no layout xml, 
assim pode-se preencher componentes de layout já diretamente, ou seja, não é necessária a chamada de setText()
de um TextView na activity pra atualizar uma label, por exemplo. Um campo de EditText, utilizando o DataBinding, 
evita-se de ter que usar o método getText() para todos os campos de um formulário.

- A arquitetura do projeto está em MVVM. Foi adotada pois mantém o projeto mais organizado, com pouco código nas 
activities. Também com o Android Architecture Components (agora dentro do Jetpack), a Google disponibilizou componentes
no qual incentiva a utilização dessa arquitetura.

- Foi utilizado o componente LiveData do Android Architecture Component para atualização na view. A Activity possui um observer
e caso ocorra alguma mudança na ViewModel, esse observer será notificado.